# Introduction

This is project used hexagonal design 

# Reference links


# Getting started

 run docker-compose to start database server anf database admin.

## What's contained in this project

- main.go - is the main definition of the service, handler and repository

## Dependencies

Install the following

- [micro](https://github.com/micro/micro)
- [protoc-gen-micro](https://github.com/micro/protoc-gen-micro)

## Run Service

```shell
 go mod tidy 
  go run main.go 
```

## Query Service

```
micro call greeter Greeter.Hello '{"name": "John"}'
```
