package repository

import "time"

type Item struct {
	ID         int64     `db:"id"`
	ProductID  int       `db:"productId"`
	SupplierID int       `db:"supplierId"`
	SKU        string    `db:"sku"`
	Quantity   int       `db:"quantity"`
	Price      float64   `db:"price"`
	Sold       int       `db:"sold"`
	Available  int       `db:"available"`
	Defective  int       `db:"defective"`
	CreatedBy  int64     `db:"createdBy"`
	UpdatedBy  int64     `db:"updatedBy"`
	CreatedAt  time.Time `db:"createdAt"`
	UpdatedAt  time.Time `db:"updatedAt"`
}

type IItem interface {
	GetBySKU(sku string) (*Item, error)
	AddStock(sku string, quantity int) (int64, error)
	RemoveStock(sku string, quantity int) (int64, error)
	UpdateStock(sku string, quantity int) (int64, error)
	CreateItem(i Item) (*Item, error)
}
