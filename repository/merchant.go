package repository

import (
	"time"
)

type Merchant struct {
	ID        int       `db:"id"`
	Title     string    `db:"title"`
	Address   string    `db:"address"`
	CreatedAt time.Time `db:"createdAt"`
	UpdatedAt time.Time `db:"updatedAt"`
	Content   string    `db:"content"`
}

type IMerchantRepo interface {
	GetAll() ([]Merchant, error)
	GetByID(id int) (*Merchant, error)
	Add(merchant Merchant) (*Merchant, error)
}
