package repository

import "github.com/jmoiron/sqlx"

type merchantDB struct {
	db *sqlx.DB
}

func NewMerchantDB(db *sqlx.DB) *merchantDB {

	return &merchantDB{db: db}
}

func (r *merchantDB) GetAll() ([]Merchant, error) {

	merchants := []Merchant{}
	query := `select * from merchant`
	if err := r.db.Select(&merchants, query); err != nil {
		return nil, err
	}
	return merchants, nil

}
func (r *merchantDB) GetByID(id int) (*Merchant, error) {
	merchant := Merchant{}
	query := `select * from merchant where id=?`
	if err := r.db.Select(&merchant, query, id); err != nil {
		return nil, err
	}
	return &merchant, nil
}
func (r *merchantDB) Add(merchant Merchant) (*Merchant, error) {
	///merchant := []Merchant{}
	query := `insert into merchant (id, title ) values ()`
	if _, err := r.db.NamedExec(query, merchant); err != nil {
		return nil, err
	}
	return &merchant, nil
}
