package repository

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lithammer/shortuuid"
)

type item struct {
	db *sqlx.DB
}

func NewItemRepo(db *sqlx.DB) *item {

	return &item{db: db}
}
func (r *item) GetBySKU(sku string) (*Item, error) {
	item := Item{}
	query := `select productId ,supplierId, sku, price, sold, available, defective from item where sku=?`
	if err := r.db.Get(&item, query, sku); err != nil {
		return nil, err
	}
	return &item, nil
}
func (r *item) AddStock(sku string, quantity int) (int64, error) {
	query := `UPDATE item set available = available + ? where sku=?`
	result, err := r.db.Exec(query, quantity, sku)
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}
func (r *item) RemoveStock(sku string, quantity int) (int64, error) {
	query := `UPDATE item set available = available - ? where sku=?`
	result, err := r.db.Exec(query, quantity, sku)
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}
func (r *item) UpdateStock(sku string, quantity int) (int64, error) {

	query := `UPDATE item set available =? where sku=?`
	result, err := r.db.Exec(query, quantity, sku)
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}
func (r *item) CreateItem(i Item) (*Item, error) {
	item := &Item{
		ProductID:  i.ProductID,
		SupplierID: i.SupplierID,
		SKU:        fmt.Sprintf("SKU-%v", shortuuid.New()),
		Quantity:   i.Quantity,
		Price:      i.Price,
		Sold:       i.Sold,
		Available:  i.Available,
		Defective:  i.Defective,
		CreatedBy:  1,
		CreatedAt:  time.Now(),
	}
	query := `insert into item (productId ,supplierId, sku, quantity, price, sold, available, defective, createdBy, createdAt) values (:productId ,:supplierId, :sku, :quantity, :price, :sold, :available, :defective, :createdBy, :createdAt)`
	if _, err := r.db.NamedExec(query, item); err != nil {
		return nil, err
	}
	return item, nil
}
