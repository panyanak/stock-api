package repository

import (
	"time"

	"github.com/jmoiron/sqlx"
)

type orderItem struct {
	db *sqlx.DB
}

func NewOrderItemRepo(db *sqlx.DB) *orderItem {
	return &orderItem{db: db}
}
func (r *orderItem) AddOrderItem(oi OrderItem) (*OrderItem, error) {
	item := &OrderItem{
		ProductID:  oi.ProductID,
		SupplierID: oi.SupplierID,
		CustomerID: oi.CustomerID,
		ItemID:     oi.ItemID,
		OrderID:    oi.OrderID,
		SKU:        oi.SKU,
		Price:      oi.Price,
		Discount:   oi.Discount,
		Quantity:   oi.Quantity,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	query := `insert into item (productId ,supplierId, customerId, itemId, orderId, sku, price, discount, quantity, createdAt, updatedAt) values (:productId, :supplierId, :customerId, :itemId, :orderId, :sku, :price, :discount, :quantity, :createdAt, :updatedAt)`
	if _, err := r.db.NamedExec(query, item); err != nil {
		return nil, err
	}
	return item, nil
}
