package repository

import "time"

type product struct {
	ID        int64     `db:"id"`
	Title     string    `db:"title"`
	Type      int       `db:"type"`
	CreatedAt time.Time `db:"createdAt"`
	UpdatedAt time.Time `db:"updatedAt"`
	Content   string    `db:"content"`
}
