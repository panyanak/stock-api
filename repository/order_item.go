package repository

import "time"

type OrderItem struct {
	ID         int       `db:"id"`
	ProductID  int       `db:"productId"`
	SupplierID int       `db:"supplierId"`
	CustomerID int       `db:"customerId"`
	ItemID     int       `db:"itemId"`
	OrderID    int       `db:"orderId"`
	SKU        string    `db:"sku"`
	Price      float64   `db:"price"`
	Discount   float64   `db:"discount"`
	Quantity   int       `db:"quantity"`
	CreatedAt  time.Time `db:"createdAt"`
	UpdatedAt  time.Time `db:"updatedAt"`
}

type IOrderItemRepo interface {
	AddOrderItem(oi OrderItem) (*OrderItem, error)
}
