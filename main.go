package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"test/stock-api/handler"
	"test/stock-api/repository"
	"test/stock-api/service"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

var db *sqlx.DB

func init() {
	initTimeZone()
	initConfig()
	db = initDatabase()
	if err := db.Ping(); err != nil {
		log.Printf("initDatabase error %#v", err.Error())
	}

}
func main() {

	merchantRepo := repository.NewMerchantDB(db)
	merchantService := service.NewMerchantService(merchantRepo)
	merchantHandler := handler.NewmerchantHandler(merchantService)

	itemRepo := repository.NewItemRepo(db)
	itemService := service.NewItemService(itemRepo)
	itemHandler := handler.NewItemHandler(itemService)

	// log.Printf("%v", merchantService.GetMerchants())
	router := mux.NewRouter()
	router.Use(commonMiddleware)

	router.HandleFunc("/merchants", merchantHandler.GetMerchants).Methods(http.MethodGet)
	router.HandleFunc("/merchant/{id:[0-9]+}", merchantHandler.GetMerchant).Methods(http.MethodGet)
	router.HandleFunc("/merchant", merchantHandler.GetMerchant).Methods(http.MethodPost)

	router.HandleFunc("/item/{sku}", itemHandler.GetItem).Methods(http.MethodGet)
	router.HandleFunc("/item/{sku}/add/{quantity:[0-9]+}", itemHandler.IncreaseItem).Methods(http.MethodPut)
	router.HandleFunc("/item/{sku}/update/{quantity:[0-9]+}", itemHandler.UpdateItem).Methods(http.MethodPut)
	router.HandleFunc("/item/{sku}/sub/{quantity:[0-9]+}", itemHandler.DecreaseItem).Methods(http.MethodPut)
	router.HandleFunc("/item", itemHandler.CreateItem).Methods(http.MethodPost)

	router.HandleFunc("/order", itemHandler.GetItem).Methods(http.MethodPost)

	router.HandleFunc("/report/{type}/merchant/{id:[0-9]+}", itemHandler.GetItem).Methods(http.MethodGet)

	log.Printf("stock service started at port " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)

}
func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}

	time.Local = ict
}

func initDatabase() *sqlx.DB {
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	)

	db, err := sqlx.Open(viper.GetString("db.driver"), dsn)
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}
