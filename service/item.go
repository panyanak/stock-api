package service

type ItemRequest struct {
	ProductID  int     `json:"productId,omitempty"`
	SupplierID int     `json:"supplierId,omitempty"`
	Quantity   int     `json:"quantity"`
	Price      float64 `json:"price"`
}

type ItemResponse struct {
	ProductID  int     `json:"productId,omitempty"`
	SupplierID int     `json:"supplierId,omitempty"`
	SKU        string  `json:"sku"`
	Price      float64 `json:"price"`
	Quantity   int     `json:"quantity,omitempty"`
	Sold       int     `json:"sold,omitempty"`
	Available  int     `json:"available"`
	Defective  int     `json:"defective,omitempty"`
}

type IItemService interface {
	GetStockBySKU(sku string) (*ItemResponse, error)
	AddStock(sku string, quantity int) (int64, error)
	SubtrackStock(sku string, quantity int) (int64, error)
	UpdateStock(sku string, quantity int) (int64, error)
	CreateStock(item ItemRequest) (*ItemResponse, error)
}
