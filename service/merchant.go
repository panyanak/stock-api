package service

import "time"

type MerchantResponse struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	Address   string    `json:"address"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Content   string    `json:"content"`
}

type IMerchantService interface {
	GetMerchants() ([]MerchantResponse, error)
	GetMerchant(id int) (*MerchantResponse, error)
	CreateMerchant(merchant MerchantResponse) (*MerchantResponse, error)
}
