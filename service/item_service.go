package service

import (
	"fmt"
	"log"
	"test/stock-api/repository"

	"github.com/lithammer/shortuuid"
)

type item struct {
	repo repository.IItem
}

func NewItemService(repo repository.IItem) *item {
	return &item{repo: repo}
}

func (s *item) GetStockBySKU(sku string) (*ItemResponse, error) {

	item, err := s.repo.GetBySKU(sku)
	if err != nil {
		log.Printf("error :: %v", err.Error())
		return nil, err
	}

	itemResp := &ItemResponse{
		ProductID:  item.ProductID,
		SupplierID: item.SupplierID,
		SKU:        item.SKU,
		Quantity:   item.Quantity,
		Sold:       item.Sold,
		Available:  item.Available,
		Defective:  item.Defective,
	}

	return itemResp, nil
}
func (s *item) AddStock(sku string, quantity int) (int64, error) {

	result, err := s.repo.AddStock(sku, quantity)
	if err != nil {
		log.Printf("error :: %v", err.Error())
		return 0, err
	}
	return result, nil
}
func (s *item) SubtrackStock(sku string, quantity int) (int64, error) {

	result, err := s.repo.RemoveStock(sku, quantity)
	if err != nil {
		log.Printf("error :: %v", err.Error())
		return 0, err
	}
	return result, nil
}
func (s *item) UpdateStock(sku string, quantity int) (int64, error) {

	result, err := s.repo.UpdateStock(sku, quantity)
	if err != nil {
		log.Printf("error :: %v", err.Error())
		return 0, err
	}
	return result, nil
}
func (s *item) CreateStock(item ItemRequest) (*ItemResponse, error) {

	itemRepo := repository.Item{
		ProductID:  item.ProductID,
		SupplierID: item.SupplierID,
		SKU:        fmt.Sprintf("SKU-%v", shortuuid.New()),
		Quantity:   item.Quantity,
		Sold:       0,
		Available:  item.Quantity,
		Defective:  0,
		Price:      item.Price,
	}
	_, err := s.repo.CreateItem(itemRepo)
	if err != nil {
		log.Printf("error :: %v", err.Error())
		return nil, err
	}

	return &ItemResponse{
		ProductID:  itemRepo.ProductID,
		SupplierID: itemRepo.SupplierID,
		SKU:        itemRepo.SKU,
		Quantity:   itemRepo.Quantity,
		Sold:       itemRepo.Sold,
		Available:  itemRepo.Available,
		Defective:  itemRepo.Defective,
		Price:      itemRepo.Price,
	}, nil
}
