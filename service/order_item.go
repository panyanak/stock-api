package service

type OrderItem struct {
	ID         int     `json:"id"`
	ProductID  int     `json:"productId"`
	SupplierID int     `json:"supplierId"`
	CustomerID int     `json:"customerId"`
	ItemID     int     `json:"itemId"`
	OrderID    int     `json:"orderId"`
	SKU        string  `json:"sku"`
	Price      float64 `json:"price"`
	Discount   float64 `json:"discount"`
	Quantity   int     `json:"quantity"`
}

type IOrderItemService interface {
	GetOrderItemByMerchantID(id int) ([]OrderItem, error)
	GetReportOrderItemByMerchantID(id int) ([]OrderItem, error)
	AddOrderItem(oi OrderItem) (*OrderItem, error)
}
