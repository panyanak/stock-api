package service

import "test/stock-api/repository"

type oderItem struct {
	itemRepo      repository.IItem
	orderItemRepo repository.IOrderItemRepo
}

func NewOrderItemService(itemRepo repository.IItem, orderItemRepo repository.IOrderItemRepo) *oderItem {
	return &oderItem{itemRepo: itemRepo, orderItemRepo: orderItemRepo}
}
