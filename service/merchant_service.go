package service

import "test/stock-api/repository"

type merchantService struct {
	repo repository.IMerchantRepo
}

func NewMerchantService(repo repository.IMerchantRepo) *merchantService {
	return &merchantService{repo: repo}
}
func (s *merchantService) GetMerchants() ([]MerchantResponse, error) {

	merchants, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}

	merchantResponses := []MerchantResponse{}
	for _, merchant := range merchants {
		merchantResponse := MerchantResponse{
			ID:        merchant.ID,
			Title:     merchant.Title,
			Address:   merchant.Address,
			CreatedAt: merchant.CreatedAt,
			UpdatedAt: merchant.UpdatedAt,
			Content:   merchant.Content,
		}
		merchantResponses = append(merchantResponses, merchantResponse)
	}
	return merchantResponses, nil
}
func (s *merchantService) GetMerchant(id int) (*MerchantResponse, error) {

	merchant, err := s.repo.GetByID(id)
	if err != nil {
		return nil, err
	}

	merchantResponse := MerchantResponse{
		ID:        merchant.ID,
		Title:     merchant.Title,
		Address:   merchant.Address,
		CreatedAt: merchant.CreatedAt,
		UpdatedAt: merchant.UpdatedAt,
		Content:   merchant.Content,
	}

	return &merchantResponse, nil
}
func (s *merchantService) CreateMerchant(merchant MerchantResponse) (*MerchantResponse, error) {
	return nil, nil
}
