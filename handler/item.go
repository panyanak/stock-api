package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"test/stock-api/service"

	"github.com/gorilla/mux"
)

type itemHandler struct {
	sv service.IItemService
}

func NewItemHandler(sv service.IItemService) *itemHandler {
	return &itemHandler{sv: sv}
}

func (h *itemHandler) GetItem(w http.ResponseWriter, r *http.Request) {
	sku := mux.Vars(r)["sku"]
	item, err := h.sv.GetStockBySKU(sku)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(item)

}
func (h *itemHandler) IncreaseItem(w http.ResponseWriter, r *http.Request) {
	sku := mux.Vars(r)["sku"]
	quantity, _ := strconv.Atoi(mux.Vars(r)["quantity"])
	item, err := h.sv.AddStock(sku, quantity)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(item)

}
func (h *itemHandler) DecreaseItem(w http.ResponseWriter, r *http.Request) {
	sku := mux.Vars(r)["sku"]
	quantity, _ := strconv.Atoi(mux.Vars(r)["quantity"])
	item, err := h.sv.SubtrackStock(sku, quantity)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(item)

}
func (h *itemHandler) UpdateItem(w http.ResponseWriter, r *http.Request) {
	sku := mux.Vars(r)["sku"]
	quantity, _ := strconv.Atoi(mux.Vars(r)["quantity"])

	item, err := h.sv.UpdateStock(sku, quantity)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(item)

}

func (h *itemHandler) CreateItem(w http.ResponseWriter, r *http.Request) {

	body := json.NewDecoder(r.Body)
	item := service.ItemRequest{}
	err := body.Decode(&item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	result, err := h.sv.CreateStock(item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)

}
