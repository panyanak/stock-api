package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"test/stock-api/service"

	"github.com/gorilla/mux"
)

type merchantHandler struct {
	merchantService service.IMerchantService
}

func NewmerchantHandler(merchantService service.IMerchantService) *merchantHandler {
	return &merchantHandler{merchantService: merchantService}
}

func (h *merchantHandler) GetMerchants(w http.ResponseWriter, r *http.Request) {
	merchants, err := h.merchantService.GetMerchants()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(merchants)

}
func (h *merchantHandler) GetMerchant(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	merchant, err := h.merchantService.GetMerchant(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(merchant)

}
func (h *merchantHandler) AddMerchant(w http.ResponseWriter, r *http.Request) {
	merchants, err := h.merchantService.GetMerchants()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(merchants)

}
