module test/stock-api

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/spf13/viper v1.9.0
)
